#include <iostream>
#include <fstream>
#include <sstream>

#include "parser.h"

namespace artools
{

    //----------------------------------------------------------------------------------------
    bool parser_read_file ( std::string filename, std::vector<std::string>& pfc,
                            bool show_lines )
    {
        std::ifstream ifile ( filename.c_str () );

        if (!ifile) {
            std::cerr << " parser_read_file :: Error opening parameter file "
                << filename << " Exiting. " << std::endl;
            return true;
        }

        do {
            std::string line = "";
            getline ( ifile, line );
            // drop comment lines
            if (line[0]!='#' && line.length ()>1) {
                pfc.push_back ( line );
                // erase things after possible second '#'
                size_t pos = line.find ( '#' );
                if (pos==std::string::npos) continue;
                pfc.back ().erase ( pos, line.length () );
            }
        } while (!ifile.eof ());

        ifile.close ();

        if (show_lines) {
            for (int l = 0; l<(int)pfc.size (); l++) std::cout << pfc[l] << std::endl;
        }

        return false;
    }

    //----------------------------------------------------------------------------------------

} // namespace artools

