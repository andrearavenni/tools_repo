#ifndef AR_TOOLS_PARSER
#define AR_TOOLS_PARSER

#include <vector>
#include <string>

namespace artools
{

// stores valid input lines from file in a vector
bool parser_read_file(std::string filename, std::vector<std::string> &pfc,
                      bool show_lines=false);

// generic read of types like int, long int, double etc
template <class T>
bool parser_read(const std::vector<std::string> &valid_lines, std::string handle, T &val, 
                 bool show_entry=false)
{
    bool found = false;
    
    for(size_t l = 0; l < valid_lines.size(); l++)
    {
        std::string line = valid_lines[l];
        size_t pos = line.find(handle);
        
// if we did not find the handle in this line then continue
        if(pos >= line.length()) continue;
        
// if we did find the handle, verify that the handle is not embedded in a longer handle
        if(pos >= 1 && line[pos-1] != ' ')
            continue;
        if(    pos+handle.length() < line.length() 
            && line[pos+handle.length()] != ' ' 
            && line[pos+handle.length()] != '=' )
            continue;
        
// if string is found, search for equal from positions on
        pos = line.find("=", pos);
// continue if at end of string
        if(pos >= line.length()) continue;
        
// increment by one to look what is after the =
        pos++;
        
// remove spaces after the equal        
        while( pos < line.length() && line[pos] == ' ' ) pos++;
// if there is no number after trailing spaces continue
        if(pos >= line.length()) continue;
            
// now at position of entry and need to convert it (everything after entry is omitted)
        std::istringstream iss(line.substr(pos));
        iss >> val;
        found = true;
    }
    
    if(show_entry==1 && found==1) std::cout << " " << handle << " = " << val << std::endl;
    
    return found;
}

} // namespace artools

#endif // AR_TOOLS_PARSER
