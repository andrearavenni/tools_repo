#ifndef AR_TOOLS_UTILITIES
#define AR_TOOLS_UTILITIES

#include <sstream>
#include <vector>
#include <iostream>
#include <fstream>
#include <chrono>
#include <utility>
#include <unistd.h>
#include <string_view>
//#include <source_location> 

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::stringstream;
using std::vector;

namespace artools
{

    //----------------------------------------------------------------------------------------
    void printfineline ();
    void printthickline ();

    //----------------------------------------------------------------------------------------
    void printerror ( string errmsg );
    void print_exception ( const std::exception& e, int level = 0 );
    string errorlog ( const std::string& loc,
                      const std::string& message = "" );
    //[TODO] update when c++20 works on the server
    //https://en.cppreference.com/w/cpp/utility/source_location
    //string errorlog(const std::string_view message,
    //                const std::source_location loc = std::source_location::current())
    void printtime ( string mess, time_t start );

    //----------------------------------------------------------------------------------------
    bool does_file_exist ( string fileName );
    void safe_open_file ( std::ofstream& file, string name );
    void safe_open_file ( std::ifstream& file, string name ); //[TODO] template to remove replicate code

    //----------------------------------------------------------------------------------------
    class chronometer
    {
    public:
        void start ( std::string );
        void print ( std::string );
        void print_and_stop ( std::string );
        void stop ( std::string );

    private:
        std::vector < std::pair < std::string, std::chrono::time_point<std::chrono::high_resolution_clock> > > points;
    };

    //----------------------------------------------------------------------------------------
    inline void ring_alarm ()
    {
        for (int i = 0; i< 25; i++)
        {
            cout << '\a' << std::flush;
            usleep ( 1.e6 );
        }
    }

    //----------------------------------------------------------------------------------------

} // namespace artools

#endif // AR_TOOLS_UTILITIES
