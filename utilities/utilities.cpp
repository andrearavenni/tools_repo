#include "utilities.h"

#include <algorithm>
#include <iomanip>
#include <assert.h>

namespace artools
{

    //========================================================================================
    void printerror ( string errmsg )
    {
        stringstream text;
        text << "ERROR: " << errmsg << std::endl << std::endl;
        cerr << text.str ();
    }

    //----------------------------------------------------------------------------------------
    // prints the explanatory string of an exception. If the exception is nested,
    // recurses to print the explanatory of the exception it holds
    void print_exception ( const std::exception& e, int level )
    {
        std::cerr << std::string ( level, ' ' ) << "exception: " << e.what () << '\n';
        try {
            std::rethrow_if_nested ( e );
        } catch (const std::exception& nestedException) {
            print_exception ( nestedException, level+1 );
        } catch (...) {}
    }
    //----------------------------------------------------------------------------------------

    string errorlog ( const std::string& loc,
                      const std::string& message )
    {
        return "'" + loc + "': " + message;
    }

    //----------------------------------------------------------------------------------------
    void printfineline ()
    {
        cout << "----------------------------------------------------------------------\n";
    }

    //----------------------------------------------------------------------------------------
    void printthickline ()
    {
        cout << "======================================================================\n";
    }

    //----------------------------------------------------------------------------------------
    bool does_file_exist ( string fileName )
    {
        std::ifstream infile ( fileName.c_str () );
        bool is_good = infile.good ();
        infile.close ();
        return is_good;
    }

    //----------------------------------------------------------------------------------------
    void safe_open_file ( std::ofstream& file, string name )
    {
        file.open ( name );
        while (!file) {
            printerror ( "cannot open \"" + name + "\"\n" +"provide a new path:" );
            name.clear ();
            std::cin >> name;

            file.open ( name );
        }
    }

    //----------------------------------------------------------------------------------------
    void safe_open_file ( std::ifstream& file, string name )
    {
        file.open ( name );
        while (!file) {
            printerror ( "cannot open \"" + name + "\"\n" +"provide a new path:" );
            name.clear ();
            std::cin >> name;

            file.open ( name );
        }
    }

    //----------------------------------------------------------------------------------------
    // Chronometer
    //----------------------------------------------------------------------------------------
    void chronometer::start ( std::string name )
    {
        auto start = std::chrono::high_resolution_clock::now ();

        points.push_back ( std::make_pair ( name, start ) );

        return;
    }

    //----------------------------------------------------------------------------------------
    void chronometer::print ( std::string name )
    {
        size_t i = 0;
        while (i < points.size ()) {
            if (name == points[i].first) {
                auto stop = std::chrono::high_resolution_clock::now ();

                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - points[i].second).count ();


                std::cout<< name << " completed in "
                    << duration/3600000 << "h "
                    << (duration/60/1000)%60 << "m "
                    << (duration/1000)%60  << "s "
                    << std::setfill ( '0' ) << std::setw ( 3 ) << duration%1000 << "\n";

                return;
            } else i++;
        }

        printerror ( "the desired cronometer has not been started" );

        return;
    }

    //----------------------------------------------------------------------------------------
    void chronometer::print_and_stop ( std::string name )
    {
        size_t i = 0;
        while (i < points.size ()) {
            if (name == points[i].first) {
                auto stop = std::chrono::high_resolution_clock::now ();

                auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop - points[i].second).count ();


                std::cout<< name << " completed in "
                    << duration/3600000 << "h "
                    << (duration/60/1000)%60 << "m "
                    << (duration/1000)%60  << "s "
                    << std::setfill ( '0' ) << std::setw ( 3 ) << duration%1000 << "\n";

                points.erase ( points.begin () + i );

                return;
            } else i++;
        }

        printerror ( "the desired cronometer has not been started" );

        return;
    }

    //----------------------------------------------------------------------------------------
    void chronometer::stop ( std::string name )
    {
        size_t i = 0;
        while (i < points.size ()) {
            if (name == points[i].first) {
                points.erase ( points.begin () + i );
            } else i++;
        }

        printerror ( "the desired cronometer has not been started" );

        return;
    }

    //----------------------------------------------------------------------------------------

} // namespace artools
