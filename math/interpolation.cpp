#include "interpolation.h"
#include "utilities.h"

#include <functional>
#include <gsl/gsl_errno.h>

namespace artools
{
    //==================================================================================================================
    spline_gslw::spline_gslw ( const spline_gslw& b )
    {
        if (b.data) {
            interpolate ( b.data->x, b.data->y, b.data->size, b.interpolation_type );
        } else {
            free ();
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    spline_gslw& spline_gslw::operator=( const spline_gslw& b )
    {
        if (b.data) {
            interpolate ( b.data->x, b.data->y, b.data->size, b.interpolation_type );
        } else {
            free ();
        }

        return *this;
    }

    //------------------------------------------------------------------------------------------------------------------
    spline_gslw::~spline_gslw ()
    {
        free ();
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline_gslw::interpolate ( const double x[], const double y[], const size_t n,
                                    const gsl_interp_type* interpolation_type )
    {
        data = gsl_spline_alloc ( interpolation_type, n );
        const int status = gsl_spline_init ( data, x, y, n );
        if (status) {
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                std::string ( "GSL error: " ) + gsl_strerror ( status )
            ) );
        }

#ifndef _USEOPENMP
        accelerator = gsl_interp_accel_alloc ();
#else
#pragma omp parallel
        {
            if (omp_get_thread_num () == 0) {
                accelerators = vector<gsl_interp_accel*> ( omp_get_max_threads () );
                for (auto& accelerator: accelerators)
                    accelerator = gsl_interp_accel_alloc ();
            }
        }
#endif
        is_initialized = true;
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline_gslw::interpolate ( const vector<double>& x, const vector<double>& y,
                                    const gsl_interp_type* interpolation_type )
    {
        if (x.size () != y.size ()) {
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                "the length of x and y array do not match"
            ) );
        }

        interpolate ( &x[0], &y[0], x.size (), interpolation_type );

        return;
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline_gslw::interpolate ( const vector<double>& x, const vector<vector<double> >& y,
                                    int entry, const gsl_interp_type* interpolation_type )
    {
        vector<double> temp_y;
        for (auto& temp : y) { temp_y.push_back ( temp[entry] ); }

        interpolate ( x, temp_y, interpolation_type );

        return;
    }

    //------------------------------------------------------------------------------------------------------------------
    double spline_gslw::evaluate ( const double x )
    {

        if (!is_initialized) {
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                "spline is not initialized"
            ) );
        }

        try {
            check_boundaries ( x );
        } catch (...) {
            std::throw_with_nested ( std::logic_error ( errorlog (
                __PRETTY_FUNCTION__
            ) ) );
        }

#ifdef _USEOPENMP
        gsl_interp_accel* accelerator = accelerators[omp_get_thread_num ()];
#endif

        return gsl_spline_eval ( data, x, accelerator );
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline_gslw::evaluate ( const double x, double& y )
    {
        y = evaluate ( x );
        return;
    }

    //------------------------------------------------------------------------------------------------------------------
    double spline_gslw::evaluate_d_dx ( const double x )
    {
        if (!is_initialized) {
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                "spline is not initialized"
            ) );
        }

        if (interpolation_type != gsl_interp_cspline) {
            // TODO check if other interpolation type work with the derivative first!
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                "the derivative for this interpolation type has not been wrapped yet"
            ) );
        }

        try {
            check_boundaries ( x );
        } catch (...) {
            std::throw_with_nested ( std::logic_error ( errorlog (
                __PRETTY_FUNCTION__
            ) ) );
        }

#ifdef _USEOPENMP
        gsl_interp_accel* accelerator = accelerators[omp_get_thread_num ()];
#endif

        return gsl_spline_eval_deriv ( data, x, accelerator );
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline_gslw::evaluate_d_dx ( const double x, double& y )
    {
        y = evaluate_d_dx ( x );
        return;
    }

    //------------------------------------------------------------------------------------------------------------------
    double spline_gslw::evaluate_dd_ddx ( const double x )
    {
        if (!is_initialized) {
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                "spline is not initialized"
            ) );
        }

        if (interpolation_type != gsl_interp_cspline) {
            // TODO check if other interpolation type work with the derivative first!
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                "the second derivative for this interpolation type has not been wrapped yet"
            ) );
        }

        try {
            check_boundaries ( x );
        } catch (...) {
            std::throw_with_nested ( std::logic_error ( errorlog (
                __PRETTY_FUNCTION__
            ) ) );
        }

#ifdef _USEOPENMP
        gsl_interp_accel* accelerator = accelerators[omp_get_thread_num ()];
#endif

        return gsl_spline_eval_deriv2 ( data, x, accelerator );
    }



    void spline_gslw::evaluate_dd_ddx ( const double x, double& y )
    {
        y = evaluate_dd_ddx ( x );
        return;
    }

    //------------------------------------------------------------------------------------------------------------------
    double spline_gslw::get_xmin () const
    {
        return data->x[0];
    }

    //------------------------------------------------------------------------------------------------------------------
    double spline_gslw::get_xmax () const
    {
        return data->x[data->size-1];
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline_gslw::check_boundaries ( const double x )
    {
        if (x > std::max ( get_xmin (), get_xmax () )) {
            stringstream mess;
            mess << "interpolation failed. Requested value is bigger than upper boundary\n"
                << " --->  Requested value= " << x
                << ", upper boundary= " << std::max ( get_xmin (), get_xmax () );

            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                mess.str ()
            ) );
        }
        if (x < std::min ( get_xmin (), get_xmax () )) {
            stringstream mess;
            mess << "interpolation failed. Requested value is smaller than lower boundary\n"
                << " --->  Requested value= " << x
                << ", lower boundary= " << std::min ( get_xmin (), get_xmax () );

            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                mess.str ()
            ) );
        }
        return;
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline_gslw::free ()
    {
        if (is_initialized) {
            gsl_spline_free ( data );
#ifndef _USEOPENMP
            gsl_interp_accel_free ( accelerator );
#else
            for (auto& accelerator: accelerators)
                gsl_interp_accel_free ( accelerator );
#endif 
        }
    }


    //==================================================================================================================
    spline2D_gslw::spline2D_gslw ( const spline2D_gslw& b )
    {
        if (b.data) {
            interpolate ( b.data->xarr, b.data->yarr, b.data->zarr,
                          b.data->interp_object.xsize, b.data->interp_object.ysize,
                          b.interpolation_type );
        } else {
            free ();
        }
    }

    //------------------------------------------------------------------------------------------------------------------
    spline2D_gslw& spline2D_gslw::operator=( const spline2D_gslw& b )
    {
        if (b.data) {
            interpolate ( b.data->xarr, b.data->yarr, b.data->zarr,
                          b.data->interp_object.xsize, b.data->interp_object.ysize,
                          b.interpolation_type );
        } else {
            free ();
        }

        return *this;
    }

    //------------------------------------------------------------------------------------------------------------------
    spline2D_gslw::~spline2D_gslw ()
    {
        free ();
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline2D_gslw::interpolate ( const double x[], const double y[], const double z[],
                                      const size_t nx, const size_t ny,
                                      const gsl_interp2d_type* interpolation_type )
    {
        data = gsl_spline2d_alloc ( interpolation_type, nx, ny );
        const int status = gsl_spline2d_init ( data, x, y, z, nx, ny );
        if (status) {
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                std::string ( "GSL error: " ) + gsl_strerror ( status )
            ) );
        }

#pragma omp parallel
#ifndef _USEOPENMP
        accelerator_x = gsl_interp_accel_alloc ();
        accelerator_y = gsl_interp_accel_alloc ();
#else
        {
            if (omp_get_thread_num () == 0) {
                int num_threads = omp_get_max_threads ();
                accelerators_x = vector<gsl_interp_accel*> ( num_threads );
                accelerators_y = vector<gsl_interp_accel*> ( num_threads );
                for (auto& accelerator: accelerators_x)
                    accelerator = gsl_interp_accel_alloc ();
                for (auto& accelerator: accelerators_y)
                    accelerator = gsl_interp_accel_alloc ();
            }
        }
#endif

        is_initialized = true;
    }


    //------------------------------------------------------------------------------------------------------------------
    void spline2D_gslw::interpolate ( const vector<double>& x, const vector<double>& y,
                                      const vector<double>& z,
                                      const gsl_interp2d_type* interpolation_type )
    {
        if (x.size () * y.size () != z.size ()) {
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                "the length of x*y and z array do not match"
            ) );
        }

        interpolate ( &x[0], &y[0], &z[0], x.size (), y.size (), interpolation_type );

        return;
    }


    //------------------------------------------------------------------------------------------------------------------
    void spline2D_gslw::interpolate ( const vector<double>& x, const vector<double>& y,
                                      const vector<vector<double> >& z,
                                      const gsl_interp2d_type* interpolation_type )
    {
        if (x.size () != z.size ()) {
            throw  std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                "the length of x and z array do not match"
            ) );
        }

        for (size_t i = 0; i < z.size (); i++) {
            if (y.size () != z[i].size ()) {
                stringstream mess;
                mess << "the lenght of y and z[" << i << "] do not match";
                throw std::logic_error ( errorlog (
                    __PRETTY_FUNCTION__,
                    mess.str ()
                ) );
            }
        }

        vector<double> zarr ( x.size ()*y.size () );
        for (size_t ix = 0; ix < x.size (); ix++) {
            for (size_t iy = 0; iy < y.size (); iy++)
                zarr[ix + x.size () * iy] = z[ix][iy];
        }

        interpolate ( &x[0], &y[0], &zarr[0], x.size (), y.size (), interpolation_type );

        return;
    }


    //------------------------------------------------------------------------------------------------------------------
    double spline2D_gslw::evaluate ( const double x, const double y )
    {
        if (!is_initialized) {
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                "spline is not initialized"
            ) );
        }
        try {
            check_boundaries ( x, y );
        } catch (...) {
            std::throw_with_nested ( std::logic_error ( errorlog (
                __PRETTY_FUNCTION__
            ) ) );
        }

#ifdef _USEOPENMP
        gsl_interp_accel* accelerator_x = accelerators_x[omp_get_thread_num ()];
        gsl_interp_accel* accelerator_y = accelerators_y[omp_get_thread_num ()];
#endif

        return gsl_spline2d_eval ( data, x, y, accelerator_x, accelerator_y );
    }




    void spline2D_gslw::evaluate ( const double x, const double y, double& z )
    {
        z = evaluate ( x, y );
        return;
    }

    //------------------------------------------------------------------------------------------------------------------
    double spline2D_gslw::get_xmin () const
    {
        return data->interp_object.xmin;
    }

    //------------------------------------------------------------------------------------------------------------------
    double spline2D_gslw::get_xmax () const
    {
        return data->interp_object.xmax;
    }

    //------------------------------------------------------------------------------------------------------------------
    double spline2D_gslw::get_ymin () const
    {
        return data->interp_object.ymin;
    }

    //------------------------------------------------------------------------------------------------------------------
    double spline2D_gslw::get_ymax () const
    {
        return data->interp_object.ymax;
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline2D_gslw::check_boundaries ( const double x, const double y )
    {
        if (x > std::max ( get_xmin (), get_xmax () )) {
            stringstream mess;
            mess << "interpolation failed. Requested x value > upper boundary\n"
                << " --->  Requested value= " << x
                << ", upper boundary= "  << std::max ( get_xmin (), get_xmax () );
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                mess.str ()
            ) );
        }
        if (x < std::min ( get_xmin (), get_xmax () )) {
            stringstream mess;
            mess << "interpolation failed. Requested x value < lower boundary\n"
                << " --->  Requested value= " << x
                << ", lower boundary= " << std::min ( get_xmin (), get_xmax () );
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                mess.str ()
            ) );
        }

        if (y > std::max ( get_ymin (), get_ymax () )) {
            stringstream mess;
            mess << "interpolation failed. Requested y value > upper boundary\n"
                << " --->  Requested value= " << y
                << ", upper boundary= " << std::max ( get_ymin (), get_ymax () );
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                mess.str ()
            ) );
        }
        if (y < std::min ( get_ymin (), get_ymax () )) {
            stringstream mess;
            mess << "interpolation failed. Requested y value < lower boundary\n"
                << " --->  Requested value= " << y
                << ", lower boundary= " << std::min ( get_ymin (), get_ymax () );
            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                mess.str ()
            ) );
        }

        return;
    }

    //------------------------------------------------------------------------------------------------------------------
    void spline2D_gslw::free ()
    {
        if (is_initialized) {
            gsl_spline2d_free ( data );
#ifndef _USEOPENMP
            gsl_interp_accel_free ( accelerator_x );
            gsl_interp_accel_free ( accelerator_y );
#else
            for (auto& accelerator: accelerators_x)
                gsl_interp_accel_free ( accelerator );
            for (auto& accelerator: accelerators_y)
                gsl_interp_accel_free ( accelerator );
#endif 
        }
    }

    //==================================================================================================================
    bool cubic_spline::interpolate ( const vector<double>& xx, const vector<double>& y )
    {
        if (xx.size () != y.size ()) {
            printerror ( "the length of x and y array do not match" );
            return true;
        }

        int n = y.size ()-1;

        coefficients.resize ( y.size () );

        if (xx[0] < xx[n]) {
            for (int i = 0; i <= n; ++i) {
                coefficients[i].x = xx[i];
                coefficients[i].a = y[i];
            }
        } else {
            for (int i = 0; i <= n; ++i) {
                coefficients[i].x = xx[n-i];
                coefficients[i].a = y[n-i];
            }
        }

        calculate_coefficients ( n );

        return false;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool cubic_spline::interpolate ( const vector<double>& xx, const vector<vector<double> >& y,
                                     int entry )
    {
        if (xx.size () != y.size ()) {
            printerror ( "the length of x and y array do not match" );
            return true;
        }

        int n = y.size ()-1;

        coefficients.resize ( y.size () );

        if (xx[0] < xx[n]) {
            for (int i = 0; i <= n; ++i) {
                coefficients[i].x = xx[i];
                coefficients[i].a = y[i][entry];
            }
        } else {
            for (int i = 0; i <= n; ++i) {
                coefficients[i].x = xx[n-i];
                coefficients[i].a = y[n-i][entry];
            }
        }

        calculate_coefficients ( n );


        return false;
    }

    //------------------------------------------------------------------------------------------------------------------
    void cubic_spline::calculate_coefficients ( const int n )
    {
        vector<double> h;

        for (int i = 0; i < n; ++i) {
            h.push_back ( coefficients[i+1].x-coefficients[i].x );
            if (coefficients[i+1].x==coefficients[i].x) {
                std::cout << coefficients[i+1].x << " " << coefficients[i].x << endl;
                printerror ( "same point is repeated" );
                exit ( 1 );
            }
        }


        vector<double> alpha;
        for (int i = 0; i < n; ++i)
            alpha.push_back ( 3*(coefficients[i+1].a-coefficients[i].a)/h[i]
                              - 3*(coefficients[i].a-coefficients[i-1].a)/h[i-1] );

        vector<double> c ( n+1 );
        vector<double> l ( n+1 );
        vector<double> mu ( n+1 );
        vector<double> z ( n+1 );
        l[0] = 1;
        mu[0] = 0;
        z[0] = 0;

        for (int i = 1; i < n; ++i) {
            l[i] = 2 *(coefficients[i+1].x-coefficients[i-1].x)-h[i-1]*mu[i-1];
            mu[i] = h[i]/l[i];
            z[i] = (alpha[i]-h[i-1]*z[i-1])/l[i];
        }

        l[n] = 1;
        z[n] = 0;
        c[n] = 0;

        for (int j = n-1; j >= 0; --j) {
            c[j] = z[j] - mu[j] * c[j+1];
            coefficients[j].b = (coefficients[j+1].a-coefficients[j].a)
                /h[j]-h[j]*(c[j+1]+2*c[j])/3;
            coefficients[j].c = c[j];
            coefficients[j].d = (c[j+1]-c[j])/3/h[j];
        }

        //     auto print = [&] (const SplineSet s)
        //     {
        //         std::cout << s.x << " " << s.a << " " << s.b << " " << s.c << " " << s.d 
        //                   << std::endl;
        //     };
        //     
        //     std::for_each(coefficients.begin(), coefficients.end(), print);

        last_i = -1;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool cubic_spline::evaluate ( const double x, double& y )
    {
        if (x> coefficients[coefficients.size ()-1].x) {
            printerror ( "interpolation failed. Requested value > upper boundary" );
            cerr << " --->  Requested value= " << x
                << ", upper boundary= " << coefficients[coefficients.size ()-1].x
                << endl;
            return true;
        }
        if (x< coefficients[0].x) {
            printerror ( "interpolation failed. Requested value < lower boundary" );
            cerr << " --->  Requested value= " << x
                << ", lower boundary= " << coefficients[0].x << endl;
            return true;
        }

        int i = last_i!=last_i ? 0 : last_i;


        if (x==coefficients[i].x) {
            y = coefficients[i].a;
            return false;
        } else if (x>coefficients[i].x) {
            // TODO guesstimate whenre the right coefficient might be!
            // i \approx (x-coefficients[i].x)/(coefficients[i+1].x-coefficients[i].x)
            while (x>coefficients[i+1].x) i++;
        } else {
            while (x<coefficients[i].x) i--;
        }

        if (x==coefficients[i].x) {
            y = coefficients[i].a;
            return false;
        } else {
            double dd = x-coefficients[i].x;
            y = coefficients[i].a + dd*(coefficients[i].b + dd*(coefficients[i].c
                                                                 + dd*coefficients[i].d));
        }
        return false;
    }

    //==================================================================================================================
    bool cubic_spline_2D::interpolate ( const vector<double>& xs,
                                        const vector<vector<double> >& ys,
                                        const vector<vector<double> >& vals )
    {
        double ymin = ys[0][0];
        double ymax = ys[0][ys.size ()-1];

        last_i = -1;

        cubic_spline temp;

        size_t imax, imin, di;
        if (xs[0]<xs[xs.size ()-1]) {
            imin = 0;
            imax = xs.size ();
            di = 1;
        } else {
            imin = xs.size () - 1;
            imax = 0;
            di = -1;
        }

        for (auto i = imin; i<imax; i += di) {
            if (ymin != ys[i][0]) {
                printerror ( "ymin does not match" );
                return true;
            }
            if (ymax != ys[i][ys.size ()-1]) {
                printerror ( "ymax does not match" );
                return true;
            }

            this->xs.push_back ( xs[i] );

            temp.interpolate ( ys[i], vals[i] );

            splines.push_back ( temp );
        }

        return false;
    }

    //------------------------------------------------------------------------------------------------------------------
    bool cubic_spline_2D::evaluate ( const double x, const double y, double& value )
    {
        if (x> xs[xs.size ()-1]) {
            printerror ( "interpolation failed. Requested x value > upper boundary" );
            cerr << " --->  Requested value= " << x
                << ", upper boundary= " << xs[xs.size ()-1] << endl;
            return true;
        }
        if (x< xs[0]) {
            printerror ( "interpolation failed. Requested x value < lower boundary" );
            cerr << " --->  Requested value= " << x
                << ", lower boundary= " << xs[0] << endl;
            return true;
        }

        int i = last_i!=last_i ? 0 : last_i;


        if (x>xs[i]) {
            // TODO guesstimate whenre the right coefficient might be!
            // i \approx (x-coefficients[i].x)/(coefficients[i+1].x-coefficients[i].x)
            while (x>xs[i+1]) i++;
        } else if (x<xs[i]) {
            while (x<xs[i]) i--;
        }

        size_t imin = std::max ( 0, i-1 );
        size_t imax = std::min ( int ( xs.size () )-1, i+2 );

        vector<double> temp_x, temp_y;
        double val;

        for (auto i = imin; i < imax; i++) {
            temp_x.push_back ( xs[i] );

            if (splines[i].evaluate ( y, val )) {
                printerror ( "" );
                return true;
            }
            temp_y.push_back ( val );
        }

        if (polint ( temp_x, temp_y, y, value )) {
            printerror ( "in polint" );
            return true;
        }

        return false;
    }

    //==================================================================================================================
    bool polint ( const vector<double> xa, const  vector<double> ya,
                  const double x, double& y )
    {
        int i, m, ns = 1;
        double den, dif, dift, ho, hp, w, dy;

        dif = fabs ( x-xa[1] );
        vector<double> c ( xa.size () );
        vector<double> d ( xa.size () );
        for (i = 1;i<=xa.size ();i++) {
            if ((dift = fabs ( x-xa[i] )) < dif) {
                ns = i;
                dif = dift;
            }
            c[i] = ya[i];
            d[i] = ya[i];
        }
        y = ya[ns--];

        for (m = 1; m<xa.size (); m++) {
            for (i = 1; i<=xa.size ()-m; i++) {
                ho = xa[i]-x;
                hp = xa[i+m]-x;
                w = c[i+1]-d[i];
                if ((den = ho-hp) == 0.0) {
                    printerror ( "two x match" );
                    return true;
                }
                den = w/den;
                d[i] = hp*den;
                c[i] = ho*den;
            }
            y += (dy = (2*ns < (xa.size ()-m) ? c[ns+1] : d[ns--]));
        }

        return false;
    }
} // namespace artools
