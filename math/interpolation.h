#ifndef AR_TOOLS_INTERPOLATION
#define AR_TOOLS_INTERPOLATION

#include <sstream>
#include <vector>
#include <iostream>
#include <math.h>

#include <gsl/gsl_spline.h>
#include <gsl/gsl_interp2d.h>
#include <gsl/gsl_spline2d.h>

#ifdef _USEOPENMP
#include <omp.h>
#endif

using std::cout;
using std::cerr;
using std::endl;
using std::string;
using std::stringstream;
using std::vector;

namespace artools
{
    //==================================================================================================================
    class spline_gslw {
    public:
        spline_gslw () {};
        spline_gslw ( const spline_gslw& b );
        spline_gslw& operator=( const spline_gslw& b );
        ~spline_gslw ();

        void interpolate ( const double x[], const double y[], const size_t n,
                           const gsl_interp_type* interpolation_type = gsl_interp_cspline );
        void interpolate ( const vector<double>& x, const vector<double>& y,
                           const gsl_interp_type* interpolation_type = gsl_interp_cspline );
        void interpolate ( const vector<double>& x, const vector< vector<double> >& y,
                           int entry,
                           const gsl_interp_type* interpolation_type = gsl_interp_cspline );

        void evaluate ( const double x, double& y );
        double evaluate ( const double x );
        void evaluate_d_dx ( const double x, double& y );
        double evaluate_d_dx ( const double x );
        void evaluate_dd_ddx ( const double x, double& y );
        double evaluate_dd_ddx ( const double x );

        double get_xmin () const;
        double get_xmax () const;

    private:
        // the actual spline class we are wrapping
        gsl_spline* data;

        // gsl accelerators, one per thread
#ifndef _USEOPENMP
        gsl_interp_accel* accelerator;
#else
        vector< gsl_interp_accel* > accelerators;
#endif

        // interpolation type TODO try akima and whatnot!
        const gsl_interp_type* interpolation_type = gsl_interp_cspline;

        // free memory
        void free ( void );
        void check_boundaries ( const double x );

        // initialization flag
        bool is_initialized = false;
    };

    //==================================================================================================================
    class spline2D_gslw {
    public:
        spline2D_gslw () {};
        spline2D_gslw ( const spline2D_gslw& b );
        spline2D_gslw& operator=( const spline2D_gslw& b );
        ~spline2D_gslw ();

        void interpolate ( const double x[], const double y[],
                           const double z[],
                           const size_t nx, const size_t ny,
                           const gsl_interp2d_type* interpolation_type = gsl_interp2d_bicubic );
        void interpolate ( const vector<double>& x, const vector<double>& y,
                           const vector<double>& z,
                           const gsl_interp2d_type* interpolation_type = gsl_interp2d_bicubic );
        void interpolate ( const vector<double>& x, const vector<double>& y,
                           const vector< vector <double> >& z,
                           const gsl_interp2d_type* interpolation_type = gsl_interp2d_bicubic );

        void evaluate ( const double x, const double y, double& z );
        double evaluate ( const double x, const double y );

        double get_xmin () const;
        double get_xmax () const;

        double get_ymin () const;
        double get_ymax () const;

    private:
        // the actual spline class we are wrapping
        gsl_spline2d* data;

        // gsl accelerators, one per thread
#ifndef _USEOPENMP
        gsl_interp_accel* accelerator_x;
        gsl_interp_accel* accelerator_y;
#else
        vector< gsl_interp_accel* > accelerators_x;
        vector< gsl_interp_accel* > accelerators_y;
#endif

        // interpolation type TODO try akima and whatnot!
        const gsl_interp2d_type* interpolation_type = gsl_interp2d_bicubic;

        // free memory
        void free ( void );
        void check_boundaries ( const double x, const double y );

        // initialization flag
        bool is_initialized = false;
    };

    //==================================================================================================================
    // Legacy code, used before wrapping the GSL splines
    // There is no guarantee I did not break it in the meantime
    struct SplineSet {
        double a;
        double b;
        double c;
        double d;
        double x;
    };

    // Credits to helium.
    // http://stackoverflow.com/questions/1204553/are-there-any-good-libraries-for-solving
    // -cubic-splines-in-c
    class cubic_spline {
    public:
        bool interpolate ( const vector<double>& x, const vector<double>& y );
        bool interpolate ( const vector<double>& x, const vector< vector<double> >& y,
                           int entry );

        bool evaluate ( const double x, double& y );

    private:
        vector<SplineSet> coefficients;

        void calculate_coefficients ( const int n );

        int last_i;
    };

    //==================================================================================================================
    // Legacy code, used before wrapping the GSL splines
    // There is no guarantee I did not break it in the meantime
    class cubic_spline_2D {
    public:
        // the sampling does not need to be constant or the tables to be squared,
        // but along one side it needs to be consistent, 
        // and extremes need to be consistent too
        // e.g.
        //
        //(1,1) (1,2) (1,3) (1,4) (1,5)
        //(2,1) (2,5)
        //(3,1) (3,4) (3,5)
        bool interpolate ( const vector<double>& xs,
                           const vector< vector<double> >& ys,
                           const vector< vector<double> >& vals );

        bool evaluate ( const double x, const double y, double& value );

    private:
        vector<double> xs;
        vector<cubic_spline> splines;

        int last_i;
    };

    //==================================================================================================================
    bool polint ( const vector<double> xa, const  vector<double> ya,
                  const double x, double& y );
} // namespace artools

#endif //AR_TOOLS_INTERPOLATION
