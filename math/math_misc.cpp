#include "math_misc.h"

#include "utilities.h"

#include <sstream>
#include <iostream>
#include <math.h>
#include <utility>
#include <unistd.h>

using std::cout;
using std::cerr;
using std::endl;
using std::stringstream;

namespace artools
{

    //--------------------------------------------------------------------------------------------------------------
    vector<double> linspace ( const double xmin, const double xmax, const int npoints )
    {
        vector<double> vec;
        double del = (xmax-xmin)/(npoints-1.);

        for (int i = 0; i < npoints-1; i++) {
            vec.push_back ( xmin + del*i );
        }

        vec.push_back ( xmax );

        return vec;
    }

    //--------------------------------------------------------------------------------------------------------------
    void linspace ( vector<double>& vec,
                    const double xmin, const double xmax, const int npoints )
    {
        double del = (xmax-xmin)/(npoints-1.);

        for (int i = 0; i < npoints-1; i++) {
            vec.push_back ( xmin + del*i );
        }

        vec.push_back ( xmax );

        return;
    }

    //--------------------------------------------------------------------------------------------------------------
    vector<double> geomspace ( const double xmin, const double xmax, const int npoints )
    {
        vector<double> vec;
        double del = pow ( (xmax/xmin), 1./(npoints-1.) );

        for (int i = 0; i < npoints-1; i++) {
            vec.push_back ( xmin * pow ( del, i ) );
        }

        vec.push_back ( xmax );

        return vec;
    }

    //--------------------------------------------------------------------------------------------------------------
    void geomspace ( vector<double>& vec,
                     const double xmin, const double xmax, const int npoints )
    {
        double del = pow ( (xmax/xmin), 1./(npoints-1.) );

        for (int i = 0; i < npoints-1; i++) {
            vec.push_back ( xmin * pow ( del, i ) );
        }

        vec.push_back ( xmax );

        return;
    }

    //--------------------------------------------------------------------------------------------------------------
    bool is_double_equal ( double a, double b, double eps )
    {
        return std::abs ( a - b ) <= eps * (std::abs ( a ) < std::abs ( b ) ? std::abs ( a ) : std::abs ( b ));
    }

    //--------------------------------------------------------------------------------------------------------------
    bool is_matrix_identity ( Eigen::MatrixXd& test, double tol )
    {
        auto compare_entry_with_tol = [&]( int i, int j, bool subtract_one = false ) {
            double val = subtract_one ? test ( i, j ) -1. : test ( i, j );

            bool outcome = val < -tol || val > +tol || val!=val;

            if (outcome) std::cout << "Entry " << i << ", " << j << " = "
                << test ( i, j ) << std::endl;

            return outcome;
            };



        for (int i = 0; i< test.rows (); i++)
            if (compare_entry_with_tol ( i, i, true )) return false;

        for (int i = 0; i< test.rows (); i++) {
            for (int j = i+1; j< test.rows (); j++) {
                if (compare_entry_with_tol ( i, j ) || compare_entry_with_tol ( j, i )) return false;
            }
        }

        return true;
    };

    //--------------------------------------------------------------------------------------------------------------
    void max_parabola_given_3points ( double x1, double y1,
                                      double x2, double y2,
                                      double x3, double y3,
                                      double& xv, double& yv )
    {
        double denom = (x1 - x2) * (x1 - x3) * (x2 - x3);
        double A = (x3 * (y2 - y1) + x2 * (y1 - y3) + x1 * (y3 - y2)) / denom;
        double B = (x3*x3 * (y1 - y2) + x2*x2 * (y3 - y1) + x1*x1 * (y2 - y3)) / denom;
        double C = (x2 * x3 * (x2 - x3) * y1
                     + x3 * x1 * (x3 - x1) * y2 + x1 * x2 * (x1 - x2) * y3) / denom;

        xv = -B / (2*A);
        yv = C - B*B / (4*A);
    }

    //--------------------------------------------------------------------------------------------------------------
    double probabilists_hermite ( unsigned int n, double x )
    {
        return std::hermite ( n, x/sqrt ( 2. ) ) * pow ( 2., -double ( n )/2. );
    }

    //--------------------------------------------------------------------------------------------------------------
    double root_finder ( std::function<double ( const double )> f,
                         double a, double b,
                         const double epsrel,
                         const string method )
    {
        double fa = f ( a );
        if (fa == 0.) {
            return a;
        }
        double fb = f ( b );
        if (fb == 0.) {
            return b;
        }
        if (fa*fb > 0.) {
            stringstream mess;
            mess << "a=" << a << ", b=" << b << " do not braket the solution";

            throw std::logic_error ( errorlog (
                __PRETTY_FUNCTION__,
                mess.str ()
            ) );
        }
        double ab = abs ( a-b );

        double c;
        double fc = fb;

        bool do_bisection = (method=="bisection");
        auto Phi = [&]( const double fb, const double fc ) {
            if (method=="Illinois") {
                return 0.5;
            } else if (method=="Pegasus") {
                return fb/(fb+fc);
            } else if (method=="Anderson&Bjork") {
                const double m = 1.-fc/fb;
                return m>0 ? m : 0.5;
            }
            };

        int i = 0;
        while (ab > epsrel*(a+b)/2.) {
            if (do_bisection) {
                c = (a+b)/2.;
                do_bisection = (method=="bisection");
            } else {
                c = (a*fb - b*fa)/(fb - fa);
            }
            fc = f ( c );
            if (fc==0.) {
                return c;
            }

            if (fc*fb < 0) {
                a = b;
                fa = fb;
            } else {
                fa *= Phi ( fb, fc );
            }
            b = c;
            fb = fc;

            const double new_ab = abs ( a-b );
            if (new_ab > 0.6*ab) {
                do_bisection = true;
            }
            ab = new_ab;
        }

        return c;
    }

} // namespace artools
