#ifndef AR_TOOLS_MATH
#define AR_TOOLS_MATH

#include <vector>

#include <Eigen/Dense>

using std::vector;
using std::string;

namespace artools
{
    //--------------------------------------------------------------------------------------------------------------
    bool is_double_equal ( const double a, const double b, const double eps );

    //--------------------------------------------------------------------------------------------------------------
    bool is_matrix_identity ( Eigen::MatrixXd& test, const double tol );

    //--------------------------------------------------------------------------------------------------------------
    vector<double> linspace ( const double xmin, const double xmax, const int npoints );
    void linspace ( vector<double>& vec,
                    const double xmin, const double xmax, const int npoints );
    vector<double> geomspace ( const double xmin, const double xmax, const int npoints );
    void geomspace ( vector<double>& vec,
                     const double xmin, const double xmax, const int npoints );

    //--------------------------------------------------------------------------------------------------------------
    void max_parabola_given_3points ( const double x1, const double y1,
                                      const double x2, const double y2,
                                      const double x3, const double y3,
                                      double& vx, double& vy );

    //--------------------------------------------------------------------------------------------------------------
    double probabilists_hermite ( unsigned int n, double x );

    //--------------------------------------------------------------------------------------------------------------
    double root_finder ( std::function<double ( const double )> f, double a, double b, double epsrel,
                         const string method );

} // namespace artools

#endif //AR_TOOLS_MATH
