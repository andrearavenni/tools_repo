# README #

A collection of C++ utilities included in multiple projects.

The parser is loosely based on the one used in an old version of [CLASS](https://lesgourg.github.io/class_public/class.html).
The cubic spline (1D and 2D) thinly wraps the GSL implementation.

math_misc.h requires [Eigen](https://eigen.tuxfamily.org/index.php?title=Main_Page)<br>
interpolation.h requires [GSL](https://www.gnu.org/software/gsl/)